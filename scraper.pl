#!/usr/bin/perl

use strict;
use Mojo::DOM;
use Mojo::Util qw(slurp trim);

opendir(my $dh, "data") || die;

while(my $file = readdir $dh) {
	next if $file =~ /^\./;

	my %result;

	my $dom = Mojo::DOM->new( slurp("data/$file") );
	
	$result{name} = $dom->at('h2')->text
		if $dom->at('h2') ;
	$result{name} =~ s/,.*//;
	
	($result{mail}) = ( $dom->to_string =~ /a href="mailto:(.+?)"/ );
	($result{phone}) = ( $dom->to_string =~ /Phone:<\/b>([-\(\)\d\s]+)<br>/ );
	($result{fax}) = ( $dom->to_string =~ /Fax:<\/b>([-\(\)\d\s]+)<br>/ );
	$result{phone} = trim($result{phone}) if $result{phone};
	$result{fax} = trim($result{fax}) if $result{fax};

	( $result{addr} ) = ( $dom->to_string =~ /<p><\/p><p>.*?<br>(.*?)<\/p>/s );
	$result{addr} =~ s/<br>//g;
	$result{addr} =~ s/\s+/ /g;
	$result{addr} = trim($result{addr}) if $result{addr};
	
	print "$result{name}\t$result{mail}\t$result{phone}\t$result{fax}\t$result{addr}\n";

	undef $dom;
}